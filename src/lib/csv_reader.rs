use std::{fs::File, io::Read};

use csv::StringRecord;

pub fn csv_read(path: &str) -> Option<String> {
    match File::open(path) {
        Ok(mut file_ready_to_read) => {
            let mut raw_data = String::new();

            match file_ready_to_read.read_to_string(&mut raw_data) {
                Ok(ok_file) => {
                    println!("{raw_data}");
                    Some(raw_data)
                }
                Err(_) => {
                    println!("Cant read file with path: {path}");
                    None
                }
            }
        }
        Err(_) => {
            println!("Cant't read file from path {path}");
            None
        }
    }
}

pub fn read_from_raw_data(data: &str) -> Option<Vec<StringRecord>> {
    // Create a new csv reader from path
    let mut reader = csv::Reader::from_reader(data.as_bytes());

    // Return a vector of StringRecords
    let mut data: Vec<StringRecord> = Vec::new();

    data.push(reader.headers().unwrap().clone());

    // reader.records() returns an interator for the records it got from the csv
    for result in reader.records() {
        let record = result.unwrap();

        data.push(record);
    }

    for record in &data {
        println!("{record:?}");
    }

    Some(data)
}
