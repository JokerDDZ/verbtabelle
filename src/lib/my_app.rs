use std::fs;

use crate::lib::csv_reader;
use crate::lib::save_data::SaveData;
use crate::lib::utils;

use std::fs::File;

use csv::StringRecord;
use egui::style::Spacing;
use egui::{Color32, FontFamily, FontId, RichText, TextStyle, Vec2};
use rand::rngs::ThreadRng;
use rand::Rng;
use serde_json;

const DEFAULT_FONT_SIZE: f32 = 20.0;

pub struct MyApp {
    pub save_data: Option<SaveData>,
    pub verb_tabelle_name: String,
    pub last_table_name: String,
    pub raw_data: Option<String>,
    pub font_size: f32,
    pub font_size_last_frame: f32,
    pub string_records: Option<Vec<StringRecord>>,
    pub rng: ThreadRng,
    pub random_vers: usize,
    pub random_column: usize,
    pub init: bool,
    pub columns_vals: Option<Vec<String>>,
    pub random_no_reps: Option<Vec<i32>>,
    pub done_random: bool,
    pub init_setup_done: bool,
    pub table_is_vaiable: bool,
}

impl MyApp {
    pub fn new() -> MyApp {
        MyApp {
            save_data: None,
            verb_tabelle_name: "Verbtabelle".to_owned(),
            last_table_name: "".to_owned(),
            raw_data: None,
            font_size: DEFAULT_FONT_SIZE,
            font_size_last_frame: DEFAULT_FONT_SIZE,
            string_records: None,
            rng: rand::thread_rng(),
            random_vers: 9999,
            random_column: 9999,
            init: false,
            columns_vals: None,
            random_no_reps: None,
            done_random: false,
            init_setup_done: false,
            table_is_vaiable: false,
        }
    }

    pub fn initialization_columns(&mut self) {
        self.init = true;
        self.done_random = false;
        self.random_no_reps = Some(Vec::new());
        self.columns_vals = Some(Vec::new());

        let string_record_result = self.string_records.as_mut().expect("No string records");

        for _ in 0..string_record_result[0].len() {
            self.columns_vals
                .as_mut()
                .expect("columns no init")
                .push("".to_string());
            println!("Add");
        }
    }

    pub fn randomize_no_reps(&mut self) {
        let string_record_result = self.string_records.as_mut().expect("No string records");

        if self.random_no_reps.as_mut().unwrap().is_empty() {
            for i in 1..string_record_result.len() {
                self.random_no_reps.as_mut().unwrap().push(i as i32);
            }
        }

        self.done_random = true;
    }

    pub fn randomize_verse(&mut self) {
        let random_index = self
            .rng
            .gen_range(0..self.random_no_reps.as_mut().unwrap().len());
        self.random_vers = self.random_no_reps.as_mut().unwrap()[random_index] as usize;
        self.random_no_reps.as_mut().unwrap().remove(random_index);
        println!("Random vers: {}", self.random_vers);
    }

    pub fn randomize_column(&mut self) {
        let string_record_result = self.string_records.as_mut().expect("No string records");

        self.random_column = self.rng.gen_range(0..string_record_result[0].len());
        println!("Random column: {}", self.random_column);
    }

    pub fn columns_set_strings_empty(&mut self) {
        let string_record_result = self.string_records.as_mut().expect("No string records");

        for i in 0..string_record_result[0].len() {
            self.columns_vals.as_mut().expect("In random, vec didnt")[i] = String::from("");
        }
    }
}

//* it's just for font stle in edit text... */
#[inline]
fn heading2() -> TextStyle {
    TextStyle::Name("Heading2".into())
}

#[inline]
fn heading3() -> TextStyle {
    TextStyle::Name("ContextHeading".into())
}

fn configure_text_styles(ctx: &egui::Context, font_size: f32) {
    use FontFamily::{Monospace, Proportional};

    let mut style = (*ctx.style()).clone();
    style.text_styles = [
        (TextStyle::Heading, FontId::new(font_size, Proportional)),
        (heading2(), FontId::new(font_size, Proportional)),
        (heading3(), FontId::new(font_size, Proportional)),
        (TextStyle::Body, FontId::new(font_size, Proportional)),
        (TextStyle::Monospace, FontId::new(font_size, Monospace)),
        (TextStyle::Button, FontId::new(font_size, Proportional)),
        (TextStyle::Small, FontId::new(font_size, Proportional)),
    ]
    .into();
    ctx.set_style(style);
}

//* it is fucked up */
impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            if !self.init_setup_done {
                self.init_setup_done = true;

                let json_file = match fs::read_to_string("save.json") {
                    Ok(file) => file,
                    Err(_) => "can't find save file to load".to_string(),
                };

                println!("json file: {json_file}");
                let fixed_data = json_file.replace("\\", "");
                let fixed_data = utils::rem_first_and_last_chars(&fixed_data);

                let save_from_json: Result<SaveData, serde_json::Error> =
                    serde_json::from_str(fixed_data);

                match save_from_json {
                    Ok(ok_val) => {
                        self.font_size = ok_val.font_size;
                        self.verb_tabelle_name = ok_val.table_name;
                    }
                    Err(_) => println!("Cant'read file correctly go on"),
                }
            }

            if self.font_size != self.font_size_last_frame {
                self.font_size_last_frame = self.font_size;
                configure_text_styles(ctx, self.font_size);
            }

            self.save_data = Some(SaveData {
                font_size: self.font_size,
                table_name: self.verb_tabelle_name.to_string(),
            });

            if self.verb_tabelle_name != self.last_table_name {
                self.last_table_name = self.verb_tabelle_name.to_string();
                self.init = false;

                let path = self.verb_tabelle_name.clone() + ".csv";

                match csv_reader::csv_read(&path) {
                    Some(real_string) => {
                        self.raw_data = Some(real_string);
                    }
                    None => {
                        println!("File with path: {path} can't be read");
                        self.raw_data = None;
                    }
                }

                match &self.raw_data {
                    Some(real_data) => {
                        self.string_records = csv_reader::read_from_raw_data(real_data);
                        self.table_is_vaiable = true;
                    }
                    None => {
                        println!("File with path: {path} can't be read");
                        self.table_is_vaiable = false;
                    }
                }
            }

            ui.heading("Verbtabelle");
            ui.horizontal(|_| {});

            ui.horizontal(|ui| {
                ui.label("Name of table: ");
                ui.text_edit_singleline(&mut self.verb_tabelle_name);
            });

            ui.horizontal(|ui| {
                let font_size_lablel_rich =
                    RichText::new("Font size").font(FontId::monospace(self.font_size));
                ui.add(
                    egui::Slider::new(&mut self.font_size, 15.0..=100.0_f32)
                        .text(font_size_lablel_rich),
                );
            });

            if self.table_is_vaiable {
                if !self.init {
                    self.initialization_columns();
                }

                if !self.done_random {
                    self.randomize_no_reps();
                    self.randomize_verse();
                    self.randomize_column();
                    self.columns_set_strings_empty();
                }

                egui::ScrollArea::both().show(ui, |ui| {
                    egui::Grid::new("Parent Grid")
                        .striped(true)
                        .min_row_height(100.0)
                        .min_col_width(300.0)
                        .spacing(Vec2::new(20.0, 20.0))
                        .show(ui, |ui| {
                            self.string_records.as_ref().expect("something")[0]
                                .into_iter()
                                .for_each(|header| {
                                    ui.horizontal(|ui| {
                                        let rich = RichText::new(header)
                                            .font(FontId::monospace(self.font_size * 1.2));

                                        ui.with_layout(
                                            egui::Layout::left_to_right(egui::Align::Center),
                                            |ui| {
                                                ui.label(rich);
                                            },
                                        );
                                    });
                                });

                            ui.end_row();

                            let mut iter = 0;

                            self.string_records.as_ref().expect("msg")[self.random_vers]
                                .into_iter()
                                .for_each(|record| {
                                    ui.horizontal(|ui| {
                                        let rich = RichText::new(record)
                                            .font(FontId::monospace(self.font_size));

                                        if iter == self.random_column {
                                            ui.with_layout(
                                                egui::Layout::top_down(egui::Align::Center),
                                                |ui| {
                                                    ui.label(rich);
                                                },
                                            );
                                        } else {
                                            let mut_string_helper =
                                                &mut self.columns_vals.as_mut().unwrap()[iter];

                                            ui.with_layout(
                                                egui::Layout::top_down(egui::Align::Center),
                                                |ui| {
                                                    ui.text_edit_singleline(mut_string_helper);
                                                },
                                            );
                                        }

                                        iter += 1;
                                    });
                                });
                            ui.end_row();
                        });

                    ui.add_space(100.0);

                    let mut everything_good = true;
                    for i in 0..self.columns_vals.as_mut().unwrap().len() {
                        let from_user = self.columns_vals.as_mut().unwrap()[i].clone();
                        let mut correct = self
                            .string_records
                            .as_ref()
                            .expect("error in checking correct")[self.random_vers][i]
                            .to_string();
                        correct = correct.trim().to_string();

                        if i == self.random_column {
                            continue;
                        }

                        if !from_user.eq(&correct) {
                            everything_good = false;

                            let rich_wrong = RichText::new(format!("Column {i} is wrong"))
                                .font(FontId::monospace(self.font_size))
                                .color(Color32::RED);

                            ui.label(rich_wrong);
                        }
                    }

                    ui.horizontal(|ui| {
                        let rich_check = RichText::new("Next")
                            .font(FontId::monospace(40.0))
                            .color(Color32::WHITE)
                            .background_color(Color32::LIGHT_GREEN);

                        //validation
                        if ui.button(rich_check).clicked() && everything_good {
                            println!("You didi it !!!");
                            self.done_random = false;
                        }

                        let rich_skip = RichText::new("Skip")
                            .font(FontId::monospace(40.0))
                            .color(Color32::WHITE)
                            .background_color(Color32::LIGHT_RED);

                        if ui.button(rich_skip).clicked() {
                            self.done_random = false;
                        }
                    });
                });
            } else {
                let rich_wrong_no_table = RichText::new("Cant read table".to_string())
                    .font(FontId::monospace(self.font_size))
                    .color(Color32::RED);
                ui.label(rich_wrong_no_table);
            }
        });
    }

    fn on_exit(&mut self, _gl: Option<&eframe::glow::Context>) {
        let json_str =
            serde_json::to_string(&self.save_data.as_ref().expect("no save data init")).unwrap();
        println!("Serialize:{json_str}");
        serde_json::to_writer(&File::create("save.json").unwrap(), &json_str);
    }
}
