use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub struct SaveData {
    pub font_size: f32,
    pub table_name: String,
}
