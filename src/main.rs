#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use eframe::egui;

use lib::my_app::*;

mod lib;


fn main() {
    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(1200.0, 800.0)),
        ..Default::default()
    };

    let my_app = MyApp::new();

    eframe::run_native(
        "Verbtabelle",
        options,
        Box::new(|_cc| Box::new(my_app)),
    );
}
